﻿using MapMaker.Components;
using MapMaker.Entities;
using MapMaker.Entities.EntityRealisations;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Nez;
using Nez.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace MapMaker
{
    public static class MapHelper
    {

        #region Fields

        public static MapMakerScene Scene;
        public static Vector2 ScreenSize { get; private set; }
        private static int curLvlIndex = 0;

        #endregion

        #region Functions

        #region Setup and app stuff

        public static void SetInstance(MapMakerScene scene)
        {
            Scene = scene;
        }
        public static void SetScreenSize(int width, int height)
        {
            ScreenSize = new Vector2(width, height);
        }

        #endregion

        #region XML functions

        public static XmlDocument LoadContentXMLFromAbsolutePath(string filename)
        {
            if (!File.Exists(filename))
                return null;
            XmlDocument xml = new XmlDocument();
            FileStream fs = File.OpenRead(filename);
            xml.Load(fs);
            fs.Close();
            return xml;
        }

        public static XmlDocument LoadContentXML(string filename)
        {
            XmlDocument xml = new XmlDocument();
            xml.Load(TitleContainer.OpenStream(Path.Combine(Nez.Core.content.RootDirectory, filename)));
            return xml;
        }

        public static XmlDocument LoadXML(string filename)
        {
            XmlDocument xml = new XmlDocument();
            using (var stream = File.OpenRead(filename))
                xml.Load(stream);
            return xml;
        }

        #endregion

        #region LevelBox and LevelBoxEntity

        public static LevelBox GetLevelBox(string path)
        {
            var doc = LoadContentXMLFromAbsolutePath(path);
            string name = Path.GetFileName(path);
            LevelBox lb = new LevelBox(doc,name);
            return lb;
        }

        public static LevelBoxEntity GetLevelBoxEntity(string path)
        {
            var lb = GetLevelBox(path);
            LevelBoxEntity lbe = new LevelBoxEntity(lb);
            return lbe;
        }

        public static LevelBoxEntity GetLevelAtPosition(Vector2 position)
        {
            return Scene.Levels.GetLevelAtPosition(position);
        }

        public static bool CanSafePlace(LevelBoxEntity level)
        {
            bool canPlace = true;
            var bds = level.GetBounds();

            foreach (LevelBoxEntity lvl in Scene.Levels.GetLevels())
            {
                if (lvl!=level && bds.Intersects(lvl.GetBounds()))
                {
                    canPlace = false;
                    break;
                }
            }

            return canPlace;
        }

        public static void GoToLevel(int index)
        {
            int lvls = Scene.Levels.GetLevels().Count;
            if (lvls < 1)
                return;

            if (index < 0 || index >= lvls)
                index = 0;

            Vector2 pos = Scene.Levels.GetLevel(index).position;
            Scene.CameraObj.getComponent<EditorCameraFollowTarget>().SetPosition((int)pos.X, (int)pos.Y);
        }

        public static void GoToNextLevel()
        {
            GoToLevel(curLvlIndex++);
            if (curLvlIndex > Scene.Levels.GetLevels().Count - 1)
                ResetCurrentLevelIndexSelection();
        }

        public static void ResetCurrentLevelIndexSelection()
        {
            curLvlIndex = 0;
        }

        public static void ShiftAllTowards(Vector2 direction)
        {
            Scene.Levels.ShiftTowards(direction);
        }

        #endregion

        #region GUI

        #region Functions

        public static GUIObjectTracker GetGUITracker()
        {
            return Scene.GuiTracker;
        }

        public static Container GetGUI()
        {
            return Scene.GuiTracker.Container;
        }
        
        public static void AddGUIElement(GUIObject obj)
        {
            Scene.GuiTracker.AddGUIObject(obj);
        }

        public static GUIObject GetGUIElementAtPosition(Vector2 position, bool onlyEnabled=false)
        {
            GUIObject obj = null;

            foreach (GUIObject gui in Scene.GuiTracker.GetGUIObjects())
            {
                if (onlyEnabled && !gui.Enabled)
                    continue;

                if ((gui.Position.X <= position.X && gui.Position.Y <= position.Y) &&
                    ((gui.Position.X + gui.Size.X) >= position.X && (gui.Position.Y + gui.Size.Y) >= position.Y))
                {
                    obj = gui;
                    break;
                }
            }

            return obj;
        }

        public static bool IsGUIElementAtPosition(Vector2 position, bool onlyEnabled=false)
        {
            return (GetGUIElementAtPosition(position, onlyEnabled) != null);
        }

        #endregion

        #region Item creations

        public static Label CreateLabel(int x, int y, string text, float scale = 2f, Color? color = null)
        {
            Label lbl = new Label(text);
            lbl.setPosition(x, y);
            lbl.setFontScale(scale);
            lbl.setFontColor(color == null ? Color.White : (Color)color);

            return lbl;
        }

        public static ImageTextButton CreateButton(int x, int y, int width, int height, Texture2D image, string text)
        {

            ImageTextButtonStyle style = new ImageTextButtonStyle();
            ImageTextButton btn = new ImageTextButton(text, style);
            btn.setPosition(x, y);
            btn.setSize(width, height);

            return btn;
        }

        #endregion

        #endregion

        #region Misc

        public static bool PointInRectangle(Vector2 point, Rectangle rect)
        {
            bool isInside = false;

            if ((rect.X <= point.X && rect.Y <= point.Y) && ((rect.X + rect.Width) >= point.X && (rect.Y + rect.Height) >= point.Y))
                isInside = true;

            return isInside;
        }

        public static bool PointInRectangle(int x, int y, Rectangle rect)
        {
            return PointInRectangle(new Vector2(x, y), rect);
        }

        public static bool PointInRectangle(int xpos, int ypos, int x, int y, int wid, int hei)
        {
            return PointInRectangle(xpos, ypos, new Rectangle(x, y, wid, hei));
        }

        public static int HorizontalAxisInput()
        {
            int res = 0;
            bool left = false;
            bool right = false;

            if (Input.isKeyDown(Keys.Left))
                left = true;
            else if (Input.isKeyDown(Keys.Right))
                right = true;

            if (left && !right)
                res = -1;
            else if (right && !left)
                res = 1;

            return res;
        }

        public static int VerticalAxisInput()
        {
            int res = 0;
            bool up = false;
            bool down = false;

            if (Input.isKeyDown(Keys.Up))
                up = true;
            else if (Input.isKeyDown(Keys.Down))
                down = true;

            if (down && !up)
                res = 1;
            else if (up && !down)
                res = -1;

            return res;
        }

        public static bool WriteFile(string filePath, string fileName, string data)
        {
            bool succ = true;

            try
            {
                using (Stream stream = File.Create(filePath + "\\" + fileName))
                {
                    using (StreamWriter sw = new StreamWriter(stream))
                    {
                        sw.Write(data);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error occured while writing to file: " + ex.Message);
                succ = false;
            }

            return succ;
        }

        public static string GetProjectFolder()
        {
            string loc = Assembly.GetEntryAssembly().Location;
            loc = loc.Substring(0, loc.LastIndexOf("\\"));
            return loc;
        }

        #endregion

        #endregion

    }
}