﻿using MapMaker.Components;
using Microsoft.Xna.Framework;
using Nez;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace MapMaker.Entities
{
    public class LevelTracker
    {

        List<LevelBoxEntity> levels;

        #region Constructors

        public LevelTracker()
        {
            levels = new List<LevelBoxEntity>();
        }
        
        #endregion
        
        #region Helper functions

        public void AddLevel(LevelBoxEntity level)
        {
            levels.addIfNotPresent(level);
        }

        public void RemoveLevel(LevelBoxEntity level)
        {
            if (levels.Contains(level))
                levels.Remove(level);
        }

        public void ClearAll()
        {
            levels.Clear();
        }

        public int GetIndex(LevelBoxEntity level)
        {
            return levels.IndexOf(level);
        }

        public LevelBoxEntity GetLevel(int index)
        {
            if (index < 0 || index >= levels.Count)
                return null;
            return levels[index];
        }

        public List<LevelBoxEntity> GetLevels()
        {
            return levels;
        }

        #endregion

        #region Functions

        public bool IsPointFree(Vector2 point)
        {
            bool isFree = true;

            foreach (LevelBoxEntity lvl in levels)
            {
                var pos = lvl.level.position;
                var size = lvl.level.Size;
                if (MapHelper.PointInRectangle(point, new Rectangle(pos.ToPoint(), size.ToPoint())))
                {
                    isFree = false;
                    break;
                }
            }

            return isFree;
        }

        public bool isRectFree(Rectangle rect)
        {
            bool isFree = true;

            Vector2 pos = Vector2.Zero;

            for (int i = rect.X; i <= (rect.X + rect.Width) && isFree; i++)
            {
                pos.X = i;
                for (int j = rect.Y; j <= (rect.Y + rect.Height) && isFree; j++)
                {
                    pos.Y = j;
                    isFree = IsPointFree(pos);
                }
            }
            
            return isFree;
        }
        
        public LevelBoxEntity GetLevelAtPosition(Vector2 position)
        {
            LevelBoxEntity inst = null;

            foreach (LevelBoxEntity lvl in levels)
            {
                var pos = lvl.level.position;
                var size = lvl.level.Size;
                if (MapHelper.PointInRectangle(position, new Rectangle(pos.ToPoint(), size.ToPoint())))
                {
                    inst = lvl;
                    break;
                }
            }

            return inst;
        }
        
        #endregion

        #region Loading and saving

        public void LoadFromFolder(string path)
        {
            ClearEverything();

            DirectoryInfo d = new DirectoryInfo(path);
            FileInfo[] Files = d.GetFiles("*.oel");

            int posx, posy, maxRoomHeightInLine;
            posx = posy = MapHelper.Scene.Settings.tileSize /2;
            maxRoomHeightInLine = 0;

            foreach (FileInfo file in Files)
            {
                try
                {
                    var ent = MapHelper.GetLevelBoxEntity(file.FullName);
                    var lvlSize = ent.level.Size;

                    // If we have no place horizontally, go to next line
                    if (posx + lvlSize.X >= MapHelper.Scene.RoomSize.X)
                    {
                        posx = 0;
                        posy += maxRoomHeightInLine + MapHelper.Scene.Settings.tileSize;
                        maxRoomHeightInLine = 0;
                    }

                    // If we have no place space verticaly, exit
                    if (posy >= MapHelper.Scene.RoomSize.Y)
                    {
                        Console.WriteLine("No more space on the screen to add the levels.");
                        break;
                    }

                    // Check if the current level is bigger than the biggest for now
                    maxRoomHeightInLine = (int)Math.Max(lvlSize.Y,maxRoomHeightInLine);

                    // Now add the entity, once we figured out where to place it
                    AddSingleLevelFile(ent, file, new Vector2(posx, posy));

                    posx += MapHelper.Scene.Settings.tileSize + (int)lvlSize.X;
                }
                catch(Exception)
                {
                    Console.WriteLine("Could not open file " + file.Name + "." + file.Extension + " because it is not a level file.");
                }
            }

            MapHelper.GoToLevel(0);
        }

        public void SaveMap(string path)
        {
            var check = Directory.CreateDirectory(path.Substring(0, path.LastIndexOf("\\")));
            FileStream stream = File.OpenWrite(path);
            var doc = GenerateXMLDocument();
            if (doc != null)
                doc.Save(stream);
            stream.Close();
        }

        public void BackUpAndSave(string path)
        {
            if (File.Exists(path))
            {
                string folder = Path.GetDirectoryName(path);
                string name = Path.GetFileNameWithoutExtension(path);
                string ext = Path.GetExtension(path);

                File.Copy(path, folder + "\\" + name + "_backup" + ext);
            }

            SaveMap(path);
        }

        public void LoadMap(string path)
        {
            var xml = MapHelper.LoadContentXMLFromAbsolutePath(path);
            if (xml == null)
                return;

            ClearEverything();
            bool placedCamera = false;

            foreach (XmlNode node in xml.DocumentElement.ChildNodes)
            {
                // Get the position inside the world
                int xpos = Int32.Parse(node.Attributes["posX"].Value);
                int ypos = Int32.Parse(node.Attributes["posY"].Value);
                
                if (!placedCamera)
                {
                    MapHelper.Scene.CameraObj.getComponent<EditorCameraFollowTarget>().SetPosition(xpos, ypos);
                    placedCamera = true;
                }

                // Create the level document from the rest
                XmlDocument clevel = new XmlDocument();
                var lvl = node.FirstChild;
                clevel.AppendChild(clevel.ImportNode(lvl,true));

                // Now create the level entity
                string fileName = node.Attributes["name"].Value;
                LevelBox lb = new LevelBox(clevel,fileName);
                LevelBoxEntity le = new LevelBoxEntity(lb);
                le.SetPosition(new Vector2(xpos, ypos));
                levels.Add(le);
                MapHelper.Scene.addEntity(le);
            }

        }

        public void RefreshMap(string mapFolder, string levelsFolder)
        {
            if (!allLevelsAreThere(mapFolder, levelsFolder))
                return;

            var origMap = MapHelper.LoadContentXMLFromAbsolutePath(mapFolder);
            if (origMap == null)
                return;

            List<string[]> lvlData = new List<string[]>();
            foreach (XmlNode node in origMap.DocumentElement.ChildNodes)
            {
                string[] dat = new string[3];
                dat[0] = node.Attributes["name"].Value;
                dat[1] = node.Attributes["posX"].Value;
                dat[2] = node.Attributes["posY"].Value;
                lvlData.Add(dat);
            }

            XmlDocument map = new XmlDocument();
            map.LoadXml("<map></map>");
            XmlNode rootNode = map.FirstChild;

            // Load every level listed from the map (in lvlData), position it and write to the new map
            List<LevelBoxEntity> lvlEnts = new List<LevelBoxEntity>();
            foreach (var pair in lvlData)
            {
                string fname = pair[0];
                var le = MapHelper.GetLevelBoxEntity(levelsFolder + fname);
                le.SetPosition(new Vector2(Int32.Parse(pair[1]), Int32.Parse(pair[2])));
                rootNode.AppendChild(rootNode.OwnerDocument.ImportNode(le.GetDataForSaving().DocumentElement, true));
            }

            // Finaly save the modified map
            FileStream stream = new FileStream(mapFolder, FileMode.Create);
            map.Save(stream);
            stream.Close();
        }

        public void RefreshAndLoad(string mapFolder, string levelsFolder)
        {
            RefreshMap(mapFolder, levelsFolder);
            LoadMap(mapFolder);
            AddNewLevelsToMap(mapFolder, levelsFolder);
        }

        #endregion

        #region Other
        
        public XmlDocument GenerateXMLDocument()
        {
            if (levels.Count < 1)
                return null;

            XmlDocument map = new XmlDocument();
            map.LoadXml("<map name=\"mapName\"></map>");
            XmlNode rootNode = map.FirstChild;

            foreach (LevelBoxEntity lvl in levels)
                rootNode.AppendChild(rootNode.OwnerDocument.ImportNode(lvl.GetDataForSaving().DocumentElement, true));
            
            return map;
        }

        private void UnloadLevelEntities()
        {
            foreach (LevelBoxEntity ent in levels)
            {
                ent.setEnabled(false);
                MapHelper.Scene.removeEntity(ent);
            }
        }

        private void ClearEverything()
        {
            UnloadLevelEntities();
            ClearAll();
            MapHelper.ResetCurrentLevelIndexSelection();
        }

        private Rectangle GetMaxBounds()
        {
            if (levels.Count < 1)
                return new Rectangle(-999,-999,-999,-999);

            Rectangle rect = new Rectangle();
            rect = levels[0].GetBounds();
            rect.Width += rect.X;
            rect.Height += rect.Y;

            for (int i = 1; i < levels.Count; i++)
            {
                var b = levels[i].GetBounds();
                if (rect.X > b.X)
                    rect.X = b.X;
                if (rect.Y > b.Y)
                    rect.Y = b.Y;
                if (rect.Width < (b.X + b.Width))
                    rect.Width = b.X + b.Width;
                if (rect.Height < (b.Y + b.Height))
                    rect.Height = b.Y + b.Width;
            }

            return rect;
        }

        internal void ShiftTowards(Vector2 direction)
        {
            Rectangle bounds = GetMaxBounds();
            int xsign = Math.Sign(direction.X);
            int ysign = Math.Sign(direction.Y);
            int xamnt = (int)Math.Abs(direction.X);
            int yamnt = (int)Math.Abs(direction.Y);

            bool doShift = true;

            // Shifting in x axis
            if (xsign == 1)
            {
                if ((bounds.Width + xamnt) >= MapHelper.Scene.RoomSize.X)
                    doShift = false;
            }
            else if (xsign == -1)
            {
                if ((bounds.X - xamnt) < 0)
                    doShift = false;
            }

            // Shifting in y axis
            if (ysign == 1)
            {
                if ((bounds.Height + yamnt) >= MapHelper.Scene.RoomSize.Y)
                    doShift = false;
            }
            else if (ysign == -1)
            {
                if ((bounds.Y - yamnt) < 0)
                    doShift = false;
            }

            if (!doShift)
                return;

            foreach (LevelBoxEntity lvl in levels)
                lvl.SetPosition(lvl.position + direction);

        }

        // Refreshing existing map levels

        private bool allLevelsAreThere(string map, string levels)
        {
            bool areThere = true;

            // Get level names from map and from folder
            var mapLevels = GetLevelNamesFromMap(map);
            var levelsInFolder = GetLevelNamesFromFolder(levels);

            // Compare the two (and write an error log if a file was not found
            using(FileStream fs = new FileStream(levels + "ERROR_LOG.txt", FileMode.Create))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine("One or more level files are missing from this folder.");
                    sw.WriteLine("Please make sure they are placed in the level folder before trying to refresh the map.");
                    sw.WriteLine();
                    sw.WriteLine("Missing files: ");
                    sw.WriteLine();
                    foreach (string lvl in mapLevels)
                    {
                        if (!levelsInFolder.Contains(lvl))
                        {
                            sw.WriteLine(lvl);
                            areThere = false;
                        }
                    }
                }
            }
            
            // If everything went fine, delete the error log
            if (areThere)
                File.Delete(levels + "ERROR_LOG.txt");

            return areThere;
        }

        private List<string> GetLevelNamesFromMap(string mapLocation)
        {
            List<string> mapLevels = new List<string>();
            var mapXml = MapHelper.LoadContentXMLFromAbsolutePath(mapLocation);
            if (mapXml == null)
                return mapLevels;
            foreach (XmlNode node in mapXml.DocumentElement.ChildNodes)
            {
                string fname = node.Attributes["name"].Value;
                if (!mapLevels.Contains(fname))
                    mapLevels.Add(fname);
            }

            return mapLevels;
        }

        private List<string> GetLevelNamesFromFolder(string folderName)
        {
            List<string> levelsInFolder = new List<string>();
            DirectoryInfo d = new DirectoryInfo(folderName);
            FileInfo[] Files = d.GetFiles("*.oel");
            foreach (FileInfo fl in Files)
            {
                string name = fl.Name;
                if (!levelsInFolder.Contains(name))
                    levelsInFolder.Add(name);
            }

            return levelsInFolder;
        }

        // Refreshing map and adding new levels

        private List<string> GetLevelsNotInMap(string mapLoc, string levelsFolder)
        {
            var allLevels = GetLevelNamesFromFolder(levelsFolder);
            var mapLvls = GetLevelNamesFromMap(mapLoc);
            var newFiles = new List<string>();

            foreach (string str in allLevels)
                if (!mapLvls.Contains(str))
                    newFiles.addIfNotPresent(str);

            return newFiles;
        }

        private List<FileInfo> GetFilesByName(string folderName, string[] fileNames)
        {
            List<string> fns = new List<string>(fileNames);
            DirectoryInfo d = new DirectoryInfo(folderName);
            FileInfo[] allFiles = d.GetFiles("*.oel");
            List<FileInfo> files = new List<FileInfo>();

            foreach (FileInfo file in allFiles)
                if (fns.Contains(file.Name))
                    files.Add(file);

            return files;
        }

        private void AddNewLevelsToMap(string mapLocation, string folderName)
        {
            var files = GetFilesByName(folderName, GetLevelsNotInMap(mapLocation, folderName).ToArray());
            if (files.Count < 1)
                return;

            Vector2? npos = new Vector2(MapHelper.Scene.Settings.tileSize / 2);
            foreach (FileInfo file in files)
            {
                try
                {
                    var ent = MapHelper.GetLevelBoxEntity(file.FullName);
                    var lvlSize = ent.level.Size;

                    npos = GetNextFreePosition(lvlSize, npos);
                    // Now add the entity, once we figured out where to place it
                    AddSingleLevelFile(ent, file, npos);
                    npos += new Vector2(MapHelper.Scene.Settings.tileSize-1, 0);
                }
                catch (Exception)
                {
                    Console.WriteLine("Could not open file " + file.Name + "." + file.Extension + " because it is not a level file.");
                }
            }

        }

        private void AddSingleLevelFile(LevelBoxEntity ent, FileInfo data, Vector2 posit)
        {
            int xPos, yPos;
            xPos = (int)posit.X;
            yPos = (int)posit.Y;
            MapHelper.Scene.addEntity(ent);
            ent.SetPosition(new Vector2(xPos, yPos));
            ent.SetFileName(data.Name);
            ent.SetFileExtension(data.Extension);
            AddLevel(ent);
        }

        private void AddSingleLevelFile(LevelBoxEntity ent, FileInfo data, Vector2? posit)
        {
            if (posit == null)
            {
                Console.WriteLine("No place where the level could be placed. Try smaller or less rooms.");
                return;
            }
            AddSingleLevelFile(ent, data, (Vector2)posit);
        }

        // Checking for free positions

        private Vector2? GetNextFreePosition(Vector2 size, Vector2? startPos)
        {
            var worldSize = MapHelper.Scene.RoomSize;
            if (startPos == null)
                startPos = Vector2.One;

            Vector2 curPos = new Vector2(((Vector2)startPos).X, ((Vector2)startPos).Y);
            int heightSkip = 0;

            Vector2 calc = Vector2.Zero;
            // While we are inside the world
            while (curPos.X <= (worldSize.X-size.X) || curPos.Y <= (worldSize.Y-size.Y))
            {
                // If we break outside the world horizontally, move down at free height
                if (curPos.X > (worldSize.X - size.X))
                {
                    curPos.X = 0;
                    curPos.Y += heightSkip;
                    heightSkip = 0;
                }

                calc = CalcMoveFarther(curPos, size);
                if (calc.X == 0 && calc.Y == 0)
                    return curPos;

                heightSkip = Math.Max(heightSkip, (int)calc.Y);
                curPos.X += calc.X;
            }
            return null;
        }

        private Vector2 CalcMoveFarther(Vector2 pos, Vector2 size)
        {
            bool isFree = true;

            Vector2 maxMov = Vector2.Zero;
            for (int i = (int)(pos.X + size.X); i >= (int)pos.X && isFree; i--)
            {
                maxMov.X = i;
                for (int j = (int)(pos.Y + size.Y); j >= (int)pos.Y && isFree; j--)
                {
                    maxMov.Y = j;
                    isFree = IsPointFree(maxMov);
                    if (!isFree)
                    {
                        LevelBoxEntity lvl = GetLevelAtPosition(maxMov);
                        maxMov = (lvl.level.position + lvl.level.Size) - pos + new Vector2(1, 0);
                    }
                }
            }

            if (isFree)
                return Vector2.Zero;
            return maxMov;
        }
        
        private bool isOutSideRoom(Vector2 pos)
        {
            var sz = MapHelper.Scene.RoomSize;
            bool isOutside = false;

            if (pos.X < 0 || pos.Y < 0)
                isOutside = true;

            if (pos.X > sz.X || pos.Y > sz.Y)
                isOutside = true;

            return isOutside;
        }

        #endregion

    }
}
