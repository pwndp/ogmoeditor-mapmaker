﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace MapMaker.Entities
{
    public class LevelBox
    {
        public Vector2 position { get; set; }

        public LevelType LevelType { get; set; }
        public XmlDocument data { get; set; }

        public Vector2 Size { get; protected set; }

        private List<Rectangle> solids;
        private List<Rectangle> exits;

        private string fileName;

        #region Constructors

        public LevelBox(XmlDocument document, string fileName) : this(document, Vector2.Zero, fileName)
        {
        }
        
        public LevelBox(XmlDocument document, Vector2 pos, string fileName)
        {
            solids = new List<Rectangle>();
            exits = new List<Rectangle>();

            position = pos;
            data = document;

            this.fileName = fileName;

            if (data == null)
                Size = Vector2.Zero;

            int w = Int32.Parse(data.DocumentElement.Attributes["width"].Value);
            int h = Int32.Parse(data.DocumentElement.Attributes["height"].Value);
            Size = new Vector2(w, h);

            LevelType = GetLevelType(data.DocumentElement.Attributes["levelType"].Value);

            getDataFromXML();
        }

        #endregion

        #region Internal

        private LevelType GetLevelType(string name)
        {
            LevelType lt = LevelType.Invalid;

            if (name == "normal")
                lt = LevelType.Normal;
            else if (name == "warpable")
                lt = LevelType.Waprable;
            else if (name == "darkness")
                lt = LevelType.Darkness;

            return lt;
        }

        private void getDataFromXML()
        {
            solids.Clear();
            exits.Clear();

            var layer = data.DocumentElement.GetElementsByTagName("BaseSolids");
            foreach (XmlNode node in data.DocumentElement.ChildNodes)
            {
                if (node.Name != "BaseSolids" && node.Name != "BaseSpawners")
                    continue;

                if (node.Name == "BaseSolids")
                {
                    // If we're getting solids
                    string mode = node.Attributes["exportMode"].Value;

                    // Get the rectangles for each mode in a different way
                    if (mode == "Rectangles")
                    {
                        foreach (XmlNode rn in node.ChildNodes)
                        {
                            int x = Int32.Parse(rn.Attributes["x"].Value);
                            int y = Int32.Parse(rn.Attributes["y"].Value);
                            int w = Int32.Parse(rn.Attributes["w"].Value);
                            int h = Int32.Parse(rn.Attributes["h"].Value);
                            solids.Add(new Rectangle(x, y, w, h));
                        }
                    }
                    else
                    {
                        throw new Exception("Implement other modes here!");
                    }
                }
                else
                { // We are getting the exits
                    foreach (XmlNode enode in node.ChildNodes)
                    {
                        if (enode.Name != "ExitMarker")
                            continue;
                        int x = Int32.Parse(enode.Attributes["x"].Value);
                        int y = Int32.Parse(enode.Attributes["y"].Value);
                        int w = Int32.Parse(enode.Attributes["width"].Value);
                        int h = Int32.Parse(enode.Attributes["height"].Value);
                        exits.Add(new Rectangle(x, y, w, h));
                    }
                }
            }
        }

        public List<Rectangle> GetSolids()
        {
            return solids;
        }

        public List<Rectangle> GetExits()
        {
            return exits;
        }

        public XmlDocument GetDataForSaving()
        {
            return data;
        }

        public string GetFileName()
        {
            return fileName;
        }

        #endregion

    }
}
