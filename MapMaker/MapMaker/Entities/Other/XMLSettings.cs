﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MapMaker
{
    /// <summary>
    /// MMSettings (Map Maker Settings) class encapsulates all the data that is shared accross the editor.
    /// </summary>
    [Serializable]
    [XmlRoot("Settings")]
    public class MMSettings : XMLDataWorker
    {

        #region Fields

        [XmlElement(elementName: "ScreenWidth")]
        public int ScreenWidth = 1280;

        [XmlElement(elementName: "ScreenHeight")]
        public int ScreenHeight = 720;

        [XmlElement(elementName:"GeneratedMapFileName")]
        public string generatedFileName = "generated_map_file";

        [XmlElement(elementName: "SourceMapFileName")]
        public string sourceFileName = "source";

        [XmlElement(elementName: "MapFolder")]
        public string mapLocation = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "\\Data";

        [XmlElement(elementName: "LevelsFolder")]
        public string levelsLocation = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "\\Data\\Levels";

        [XmlElement(elementName: "EnableManualSettingsRefreshing")]
        public bool manualFileRefreshingEnabled = false;

        [XmlElement(elementName:"RefreshInterval")]
        public float settingsRefreshInterval = 2f;

        [XmlElement(elementName:"TileSize")]
        public int tileSize = 8;

        [XmlElement(elementName: "TileSizeIncrement")]
        public int tileSizeIncrement = 2;

        [XmlElement(elementName: "CameraSpeed")]
        public int cameraSpeed = 10;

        [XmlElement(elementName: "ZoomInOutSpeed")]
        public float ZoomOutSpeed = 1f;
        
        #endregion

        #region Constructors

        public MMSettings()
        {
            SetFileName("Settings.xml");
        }

        #endregion

        #region Overriden

        public override void OnLoad(object xmlSerializer)
        {
            try
            {
                var file = (MMSettings)xmlSerializer;

                // Size of the tiles
                tileSize = file.tileSize;
                if (tileSize < 1)
                    tileSize = 8;
                else if (tileSize > 1024)
                    tileSize = 1024;

                // Settings refresh interval
                settingsRefreshInterval = file.settingsRefreshInterval;
                if (settingsRefreshInterval < 1f)
                    settingsRefreshInterval = 1f;
                else if (settingsRefreshInterval > 10f)
                    settingsRefreshInterval = 3f;
                manualFileRefreshingEnabled = file.manualFileRefreshingEnabled;

                // Tile size increment
                tileSizeIncrement = file.tileSizeIncrement;
                if (tileSizeIncrement < 1)
                    tileSizeIncrement = 1;
                else if (tileSizeIncrement > 255)
                    tileSizeIncrement = 255;

                // Camera movement speed
                cameraSpeed = file.cameraSpeed;
                if (cameraSpeed < 1)
                    cameraSpeed = 1;
                else if (cameraSpeed > 50)
                    cameraSpeed = 50;

                // Zoom in and zoom out speed
                ZoomOutSpeed = file.ZoomOutSpeed;
                if (ZoomOutSpeed <= 0)
                    ZoomOutSpeed = 0.01f;
                else if (ZoomOutSpeed > 2)
                    ZoomOutSpeed = 2;

                // Input and output file names
                generatedFileName = file.generatedFileName;
                if (string.IsNullOrWhiteSpace(generatedFileName))
                    generatedFileName = "generated_map_file";
                sourceFileName = file.sourceFileName;
                if (string.IsNullOrWhiteSpace(sourceFileName))
                    sourceFileName = "source";

                // Screen size
                ScreenWidth = file.ScreenWidth;
                if (ScreenWidth < 320)
                    ScreenWidth = 320;
                ScreenHeight = file.ScreenHeight;
                if (ScreenHeight < 180)
                    ScreenHeight = 180;
                

                // Map folder
                mapLocation = file.mapLocation;
                if (string.IsNullOrWhiteSpace(mapLocation))
                    mapLocation = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "\\Data";
                if (!Directory.Exists(mapLocation))
                    Directory.CreateDirectory(mapLocation);
                // Levels folder
                levelsLocation = file.levelsLocation;
                if (string.IsNullOrWhiteSpace(levelsLocation))
                    levelsLocation = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "\\Data\\Levels\\";
                if (!Directory.Exists(levelsLocation))
                    Directory.CreateDirectory(levelsLocation);

            }
            catch(Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("An error occured trying to read/write file:");
                sb.AppendLine();
                sb.Append(ex.Message);
                if (ex.InnerException != null)
                {
                    sb.AppendLine();
                    sb.AppendLine("Additional info: ");
                    sb.Append(ex.InnerException.Message);
                }

                MapHelper.WriteFile(Assembly.GetEntryAssembly().Location, "ERROR_LOG.txt", sb.ToString());
            }
        }

        #endregion

    }
}
