﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Nez;
using Nez.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapMaker.Entities.EntityRealisations
{
    public class GUIObject: Entity, IMouseLeftInteractive, IMouseRightInteractive, IHoverable
    {

        #region Fields
        
        public Type ItemType { get; private set; }
        public object Item { get; private set; }

        public bool Enabled { get; set; }
        public int ID { get; private set; }
        public Vector2 Position;
        public Vector2 Size;
        
        #endregion

        #region Constructors

        public GUIObject(Vector2 size): this("gui_object",Vector2.Zero,size)
        {
        }

        public GUIObject(Vector2 pos, Vector2 size): this("gui_object",pos,size)
        {
        }

        public GUIObject(string name, Vector2 pos, Vector2 size): base(name)
        {
            Position = pos;
            Size = size;
            ID = 0;
            Enabled = true;
        }

        #endregion

        #region Functions

        public void SetID(int id)
        {
            ID = id;
        }

        public void AssignItem(Table item)
        {
            ItemType = item.GetType();
            Item = item;
        }

        public void AssignItem(Element item)
        {
            ItemType = item.GetType();
            Item = item;
        }
        
        #endregion

        #region Virtuals

        public virtual void OnMouseLeftClicked(Vector2 position)
        {
        }

        public virtual void OnMouseLeftHeld(Vector2 position)
        {
        }

        public virtual void OnMouseLeftReleased(Vector2 position)
        {
        }

        public virtual void OnMouseRightClicked(Vector2 position)
        {
        }

        public virtual void OnMouseRightHeld(Vector2 position)
        {
        }

        public virtual void OnMouseRightReleased(Vector2 position)
        {
        }

        public virtual void OnMouseHoverOver(Vector2 position)
        {
        }

        #endregion
        
    }
}
