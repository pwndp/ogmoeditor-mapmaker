﻿using MapMaker.Components;
using Microsoft.Xna.Framework;
using Nez;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace MapMaker.Entities
{
    public class LevelBoxEntity: Entity
    {

        #region Fields

        LevelBoxDrawerComp drawer;
        public LevelBox level { get; protected set; }
        public List<Rectangle> solids;
        public List<Rectangle> exits;

        internal string fileName;
        internal string extension;
        private Vector2 lastSafe;
        private int origDepth;

        #endregion

        public LevelBoxEntity(LevelBox levelbox): base("lvl_box")
        {
            fileName = "";
            level = levelbox;
            lastSafe = position;

            solids = level.GetSolids();
            exits = level.GetExits();

            drawer=addComponent<LevelBoxDrawerComp>();
            origDepth = drawer.renderLayer;
        }

        #region Functions

        public void SetFileName(string name)
        {
            fileName = name;
        }

        public void SetFileExtension(string ext)
        {
            extension = ext;
        }

        public void SetPosition(Vector2 pos)
        {
            if (pos.X < 0)
                pos.X = 0;
            if (pos.Y < 0)
                pos.Y = 0;

            if ((pos.X + level.Size.X >= MapHelper.Scene.RoomSize.X)
                || (pos.Y + level.Size.Y >= MapHelper.Scene.RoomSize.Y))
                return;
            
            level.position = pos;
            this.position = pos;
        }
       
        public Rectangle GetBounds()
        {
            return new Rectangle(level.position.ToPoint(), level.Size.ToPoint());
        }

        public void Take()
        {
            lastSafe = position;
            origDepth = drawer.renderLayer;
            drawer.setRenderLayer(-9999);
        }

        public void Drop()
        {
            drawer.setRenderLayer(origDepth);
            if (position == lastSafe || !MapHelper.CanSafePlace(this))
            {
                SetPosition(lastSafe);
                return;
            }
        }

        public XmlDocument GetDataForSaving()
        {
            var data = level.GetDataForSaving();

            XmlDocument xml = new XmlDocument();
            string baseString = "<levelData name=\"" + level.GetFileName() +"\" posX=\"" + position.X + "\" posY=\"" + position.Y + "\"></levelData>";
            xml.LoadXml(baseString);
            var rootNode = xml.FirstChild;
            rootNode.AppendChild(rootNode.OwnerDocument.ImportNode(data.DocumentElement, true));

            return xml;
        }

        #endregion

    }
}
