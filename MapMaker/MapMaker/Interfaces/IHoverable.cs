﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapMaker
{
    public interface IHoverable
    {

        void OnMouseHoverOver(Vector2 position);

    }
}
