﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapMaker
{
    public interface IMouseRightInteractive
    {

        void OnMouseRightClicked(Vector2 position);
        void OnMouseRightHeld(Vector2 position);
        void OnMouseRightReleased(Vector2 position);

    }
}
