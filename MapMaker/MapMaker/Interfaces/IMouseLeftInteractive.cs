﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapMaker
{
    public interface IMouseLeftInteractive
    {

        void OnMouseLeftClicked(Vector2 position);
        void OnMouseLeftHeld(Vector2 position);
        void OnMouseLeftReleased(Vector2 position);

    }
}
