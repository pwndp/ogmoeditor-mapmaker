﻿using MapMaker.Components;
using MapMaker.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Nez;
using System.IO;
using System.Text;

namespace MapMaker
{
    public class MapMakerScene : Scene
    {

        #region Fields

        #region References

        public MMSettings Settings;

        ScreenSpaceRenderer screenRenderer;
        RenderLayerRenderer gridLayerRenderer;
        RenderLayerExcludeRenderer levelRenderer;

        internal MouseFollower mousePosition;
        internal Entity CameraObj;
        internal LevelTracker Levels;
        internal GUIObjectTracker GuiTracker;

        #endregion

        #region Readonly, const and enums

        public readonly Vector2 RoomSize = new Vector2(38400, 21600);
        public readonly int gridRenderLayer = -99999; // Layer of the grid (below everything)
        public readonly int uiRenderLayer = 99999; // Everything below are object layers

        #endregion

        #endregion

        #region Constructors and overrides

        public MapMakerScene()
        {
            clearColor = Color.Black;
        }

        public override void initialize()
        {
            base.initialize();

            // Prepare the settings and update the settings after the each updateInterval amount
            Settings = new MMSettings();
            Settings.Load();
            if (!Settings.manualFileRefreshingEnabled)
                Core.schedule(Settings.settingsRefreshInterval, true, t => { Settings.Save(); });

            // Initialize the helper
            MapHelper.SetInstance(this);

            // Add renderers
            gridLayerRenderer = new RenderLayerRenderer(0, gridRenderLayer); 
            screenRenderer = new ScreenSpaceRenderer(2, uiRenderLayer); // Renders the ui at a specific layer
            levelRenderer = new RenderLayerExcludeRenderer(1, uiRenderLayer, gridRenderLayer); // Renders everything on screen except the ui
            addRenderer(screenRenderer);
            addRenderer(levelRenderer);

            // Add helper entities
            Levels = new LevelTracker();
            createHelpers(); 
        }

        public void removeEntity(Entity entity)
        {
            if (entities.contains(entity))
                entities.remove(entity);
        }

        #endregion

        #region Functions

        private void createHelpers()
        {
            // Add camera target
            CameraObj = createEntity("camera_target");
            CameraObj.addComponent<EditorCameraFollowTarget>();
            CameraObj.addComponent(GuiTracker = new GUIObjectTracker());

            // Add grid
            var grid = createEntity("grid");
            Grid gr = new Grid();
            grid.addComponent(gr);
            gr.SetGridColor(Color.DarkGray);
            gr.SetGridAlpha(0.05f);

            // Mouse
            var mp = createEntity("cursor");
            mp.addComponent(mousePosition = new MouseFollower());
            mp.addComponent(new MouseInteract());

            // Create files and folders if missing
            Core.schedule(Settings.settingsRefreshInterval, t =>
            {
                if (!File.Exists(MapHelper.GetProjectFolder() + "\\ReadMe.txt"))
                    WriteReadme();
                if (!Directory.Exists(MapHelper.GetProjectFolder() + "\\Data"))
                    Directory.CreateDirectory(MapHelper.GetProjectFolder() + "\\Data");
                if (!Directory.Exists(MapHelper.GetProjectFolder() + "\\Data\\Levels"))
                    Directory.CreateDirectory(MapHelper.GetProjectFolder() + "\\Data\\Levels");
            });
        }

        private void WriteReadme()
        {
            string appVersion = "Map Maker v1.0.3";

            string[] text0 =
            {
                string.Empty,
                "You can edit the Settings.xml file and change the location of the map and the levels folder.",
                "Also, you can change the name of the generated map and the file that is being loaded into the map maker.",
                "The RefreshInterval is at which rate (in seconds) is the settings being updated (if changed).",
                "You can also manually set the tile size inside the range [1,1024]."
            };

            string[] text1 =
            {
                "To create a new map from level (oel) files, place those files in the specified LevelsFolder folder.",
                "From the editor, you can load those levels by pressing ctrl+shift+L.",
                "You can also load a previously created map, by placing it in the specified MapFolder folder and while in editor pressing shift+L.",
                "The loaded map MUST be named the same as the name in the settings or else it won't load.",
                "To save current map, while in the editor, press shift+S or ctrl+S to save.",
                "The generated map will be created inside the specified MapFolder folder with the specified name as in GeneratedMapFileName in Settings.",
                "You can also refresh the map source file if any of the level files has been modified by pressing shift+R",
                "(note: the levels from the map must be inside the LevelsFolder folder or it will fail.",
                "Also if there are new levels added, they will also be loaded, but won't stay there unless you save the map).",
                "If some of the level files are missing from the LevelsFolder folder, an error log will list them (so better check them out).",
                "If no MapFolder or LevelsFolder is specified, the default folder will be used (the 'Data' folder inside the folder of the executable)."
            };

            string[] text2 =
            {
                "For the levels to be able to be displayed correctly, they should have minimum two layers: BaseSolids and BaseSpawners.",
                "The BaseSolids should be a grid layer with Rectangles export mode.",
                "The BaseSpawners should be an entity layer with at least the ExitMarker entity representing the exits."
            };

            string[] final =
            {
            string.Empty,
            "-----------------------------------------------------------------------------------------------",
            string.Empty,
            "Controls: ",
            string.Empty
        };
            
            string[][] txts = { text0, text1, text2, final };

            /// Create the string
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(appVersion);
            for (int i = 0; i < txts.Length; i++)
            {
                foreach (string str in txts[i])
                    sb.AppendLine(str);
                sb.AppendLine();
            }

            foreach (string str in CameraObj.getComponent<GUIObjectTracker>().GuiHelp.GetLabels())
                sb.AppendLine(str);

            MapHelper.WriteFile(MapHelper.GetProjectFolder(), "ReadMe.txt", sb.ToString());
        }

        public override void update()
        {
            base.update();

            if (Input.isKeyDown(Keys.LeftShift) || Input.isKeyDown(Keys.RightShift))
            {

                if (Settings.manualFileRefreshingEnabled && Input.isKeyPressed(Keys.F10))
                    resetCurrentScene();

                if (Input.isKeyPressed(Keys.Multiply))
                    Settings.tileSize+=Settings.tileSizeIncrement;
                else if (Input.isKeyPressed(Keys.Divide))
                    Settings.tileSize-=Settings.tileSizeIncrement;

                if (Input.isKeyPressed(Keys.L))
                {
                    bool ctrl = Input.isKeyDown(Keys.LeftControl) || Input.isKeyDown(Keys.RightControl);
                    if (ctrl)
                        Levels.LoadFromFolder(Settings.levelsLocation+"\\");
                    else
                        Levels.LoadMap(Settings.mapLocation + "\\" + Settings.sourceFileName + ".map");
                }

                if (Input.isKeyPressed(Keys.S))
                {
                    bool ctrl = Input.isKeyDown(Keys.LeftControl) || Input.isKeyDown(Keys.RightControl);
                    if (!ctrl)
                        Levels.SaveMap(Settings.mapLocation + "\\" + Settings.generatedFileName + ".map");
                    else
                        Levels.BackUpAndSave(Settings.mapLocation + "\\" + Settings.sourceFileName + ".map");
                }

                if (Input.isKeyPressed(Keys.R))
                    Levels.RefreshAndLoad(Settings.mapLocation + "\\" + Settings.sourceFileName + ".map", Settings.levelsLocation+"\\");

                if (Input.isKeyPressed(Keys.X))
                    MapHelper.GoToNextLevel();

            }
            else
            {
                bool ctrl = Input.isKeyDown(Keys.LeftControl) || Input.isKeyDown(Keys.RightControl);
                if (ctrl && Input.isKeyPressed(Keys.S))
                    Levels.SaveMap(Settings.mapLocation + "\\" + Settings.generatedFileName + ".map");
            }

            // Limit the tilesize
            if (Settings.tileSize < 1)
                Settings.tileSize = 1;
            else if (Settings.tileSize > 1024)
                Settings.tileSize = 1024;
        }

        private void resetCurrentScene()
        {
            int width = Settings.ScreenWidth; // 1280
            int height = Settings.ScreenHeight; // 720
            MapHelper.SetScreenSize(width, height);
            Scene.setDefaultDesignResolution(width, height, Scene.SceneResolutionPolicy.BestFit, width/16, height/15);
            MapMakerScene scene = new MapMakerScene();
            Core.scene = scene as MapMakerScene;
            Core.exitOnEscapeKeypress = false;
        }

        #endregion

    }
}

