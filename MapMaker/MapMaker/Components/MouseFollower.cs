﻿using Microsoft.Xna.Framework;
using Nez;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapMaker.Components
{
    public class MouseFollower : Component, IUpdatable
    {
        MapMakerScene editor;
        public int tileXPos, tileYPos;
        public int rawXPos, rawYPos;
        public Collider MouseCollider;
        public CollisionResult MouseCollisions;
        public bool overGuiElement;
        public override void onAddedToEntity()
        {
            base.onAddedToEntity();
            editor = entity.scene as MapMakerScene;
            MouseCollider = entity.addComponent(new BoxCollider(editor.Settings.tileSize, editor.Settings.tileSize));
            MouseCollider.collidesWithLayers = Physics.allLayers;
            tileXPos = tileYPos = 0;
            rawXPos = rawYPos = 0;
        }

        public Vector2 mousePosition()
        {
            return new Vector2(rawXPos, rawYPos);
        }

        public Vector2 mousePositionFixed()
        {
            int ts = editor.Settings.tileSize;
            int ntx = (tileXPos / ts) * ts + ts / 2;
            int nty = (tileYPos / ts) * ts + ts / 2;
            return new Vector2(ntx, nty);
        }

        public void update()
        {
            Vector2 rawPos = editor.camera.mouseToWorldPoint();
            
            entity.position = rawPos;
            rawXPos = (int)rawPos.X;
            rawYPos = (int)rawPos.Y;

            // Update entity position based on the mouse position, but inside tiles
            int nx = (int)rawPos.X;
            int ny = (int)rawPos.Y;
            int ts = editor.Settings.tileSize;
            tileXPos = (nx / ts) * ts + ts / 2;
            tileYPos = (ny / ts) * ts + ts / 2;

            entity.position = new Vector2(tileXPos, tileYPos);
        }


    }
}
