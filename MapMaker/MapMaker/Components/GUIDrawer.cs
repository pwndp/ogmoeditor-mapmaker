﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Nez;
using Nez.Console;
using Nez.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapMaker.Components
{
    public class GUIDrawer: Component, IUpdatable
    {

        #region Fields

        List<Label> helpLabels;

        #endregion

        #region Overriden
        
        public GUIDrawer()
        {
            helpLabels = new List<Label>();
        }

        public override void onAddedToEntity()
        {
            base.onAddedToEntity();

            AddLabel("directional arrows to move around the world (shift or shift+ctrl speed up)");
            AddLabel("+ or - to zoom in/out of the world (shift or shift+ctrl speed up)");
            AddLabel("shift+S or ctrl+S to save map without backup and ctrl+shift+S to save with backup and overwrite source file");
            AddLabel("shift+L to load levels from map and ctrl+shift+L to load levels from folder");
            AddLabel("shift+R to refresh map data (if the level files are modified)");
            AddLabel("shift+X to cycle through levels one by one");
            AddLabel("alt + directional arrows to shift all rooms in a direction");
            AddLabel("ctrl+shift+G to toggle grid visibility");
            AddLabel("shift+* or shift+/ to increase/decrease grid size");
            AddLabel("shift+F10 to reset editor and apply settings (if enabled in settings beforehand)");

            ToggleVisibility();
        }
        
        #endregion

        #region Functions

        private void AddLabel(string text)
        {
            var cont = MapHelper.GetGUI();
            var lbl = MapHelper.CreateLabel(16, 16+(16 + 8)*helpLabels.Count, text, color: Color.Red);
            helpLabels.addIfNotPresent(lbl);
            cont.addElement(lbl);
        }

        public void ToggleVisibility()
        {
            foreach (Label lbl in helpLabels)
                lbl.setVisible(!lbl.isVisible());
        }

        public void update()
        {
            // Disable toggling help (for now)
            if (false && Input.isKeyPressed(Keys.H))
                ToggleVisibility();
        }

        public List<string> GetLabels()
        {
            List<string> strs = new List<string>();

            foreach (Label lbl in helpLabels)
                strs.Add(lbl.getText());

            return strs;
        }

        #endregion
    }
}
