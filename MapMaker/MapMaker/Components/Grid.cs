﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Nez;
using Nez.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapMaker.Components
{
    public class Grid : RenderableComponent, IUpdatable
    {
        MapMakerScene eScene;
        Color origCol;
        Color _gridColor;
        bool shouldRender = false;

        public override RectangleF bounds
        {
            get
            {
                if (_areBoundsDirty)
                {
                    setBounds();
                    _areBoundsDirty = false;
                }

                return _bounds;
            }
        }

        public void resetGridSize()
        {
            setBounds();
        }

        public override void onAddedToEntity()
        {
            base.onAddedToEntity();

            eScene = entity.scene as MapMakerScene;
            layerDepth = -1000;
            SetGridAlpha(1f);
        }
        
        public void update()
        {
            if (Input.isKeyDown(Keys.LeftShift) && Input.isKeyDown(Keys.LeftControl) && Input.isKeyPressed(Keys.G))
                shouldRender = !shouldRender;
        }

        public void SetGridColor(Color color)
        {
            origCol = color;
        }

        public void SetGridAlpha(float amount)
        {
            _gridColor = origCol * amount;
        }

        public override void render(Graphics graphics, Camera camera)
        {
            if (!shouldRender)
                return;

            int thickness = 1;

            int ts = eScene.Settings.tileSize;
            int wid = ((int)Math.Floor(eScene.RoomSize.X / (float)ts) + 1) * ts;
            int hei = ((int)Math.Floor(eScene.RoomSize.Y / (float)ts) + 1) * ts;

            // If tile size is smaller that 4px, dont draw it
            if (ts < 4)
                return;

            for (int i = -ts/2; i < wid + eScene.Settings.tileSize * 6; i += ts)
                graphics.batcher.drawLine(new Vector2(i, 0), new Vector2(i, hei), _gridColor, thickness);

            for (int i = -ts / 2; i < hei + eScene.Settings.tileSize * 6; i += ts)
                graphics.batcher.drawLine(new Vector2(0, i), new Vector2(wid, i), _gridColor, thickness);

        }

        private void setBounds()
        {
            _bounds.calculateBounds(new Vector2(0, 0), new Vector2(0, 0), new Vector2(0, 0), new Vector2(1, 1), 0f, eScene.RoomSize.X + 48, eScene.RoomSize.Y + 48);
        }

    }
}
