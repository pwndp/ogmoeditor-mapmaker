﻿using MapMaker.Entities.EntityRealisations;
using Nez;
using Nez.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapMaker.Components
{
    public class GUIObjectTracker : Component
    {

        #region Fields

        internal GUIDrawer GuiHelp;
        List<GUIObject> guiObjects;
        MapMakerScene editor;
        UICanvas uiCanvas;
        Stage stage;
        internal Container Container;

        #endregion

        #region Constructors and helpers

        public GUIObjectTracker()
        {
            guiObjects = new List<GUIObject>();
        }

        public List<GUIObject> GetGUIObjects()
        {
            return guiObjects;
        }

        public void AddGUIObject(GUIObject obj)
        {
            guiObjects.addIfNotPresent(obj);
        }

        public void RemoveGUIObject(GUIObject obj)
        {
            if (guiObjects.Contains(obj))
                guiObjects.Remove(obj);
        }

        #endregion

        #region Overriden

        public override void onAddedToEntity()
        {
            base.onAddedToEntity();
            editor = entity.scene as MapMakerScene;

            var uiEnt =editor.createEntity("ui-gui");
            uiCanvas = new UICanvas();
            uiCanvas.renderLayer = editor.uiRenderLayer;
            stage = uiCanvas.stage;
            uiEnt.addComponent(uiCanvas);
            Container = stage.addElement(new Container());
            GuiHelp = uiEnt.addComponent<GUIDrawer>();
        }
        
        #endregion

        #region Other functions

        public GUIObject[] GetGUIObjectsWithID(int id)
        {
            List<GUIObject> found = new List<GUIObject>();
            foreach (var obj in guiObjects)
                if (obj.ID == id)
                    found.Add(obj);

            return found.ToArray();
        }

        public T[] GetGUIObjectsOfType<T>() where T : GUIObject
        {
            List<T> found = new List<T>();
            foreach (var obj in guiObjects)
                if (obj is T)
                    found.Add(obj as T);

            return found.ToArray();
        }

        public T[] GetGUIObjectsOfType<T>(int id) where T:GUIObject
        {
            List<T> found = new List<T>();
            foreach (var obj in guiObjects)
                if (obj is T && obj.ID==id)
                    found.Add(obj as T);

            return found.ToArray();
        }
        
        #endregion

    }
}
