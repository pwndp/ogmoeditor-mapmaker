﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Nez;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapMaker.Components
{
    public class EditorCameraFollowTarget : Component, IUpdatable
    {
        MapMakerScene editor;
        FollowCamera follower;
        float zoomAmount = 0;
        float zoomApproach = 0.002f;
        public Vector2 MoveSpeed { get; set; }

        public override void onAddedToEntity()
        {
            base.onAddedToEntity();

            editor = entity.scene as MapMakerScene;
            entity.position = MapHelper.ScreenSize / 2;
            // Add a follower that follows the entity this component belongs to
            follower = new FollowCamera(entity, FollowCamera.CameraStyle.LockOn);
            editor.camera.addComponent(follower);
            editor.camera.minimumZoom = 0.1f;
            zoomApproach *= editor.Settings.ZoomOutSpeed;
            MoveSpeed = new Vector2(editor.Settings.cameraSpeed);
        }

        public void update()
        {
            bool shiftPressed = Input.isKeyDown(Keys.LeftShift) || Input.isKeyDown(Keys.RightShift);
            bool ctrlPressed = Input.isKeyDown(Keys.LeftControl) || Input.isKeyDown(Keys.RightControl);
            bool altPressed = Input.isKeyDown(Keys.LeftAlt) || Input.isKeyDown(Keys.RightAlt);

            #region Zoom
            
            if (!altPressed && Input.isKeyDown(Keys.Add))
            {
                int amplif = 1;

                if (shiftPressed && ctrlPressed)
                    amplif = 16;
                else if (shiftPressed)
                    amplif = 4;
                
                zoomAmount += amplif * zoomApproach;
            }
            
            if (!altPressed && Input.isKeyDown(Keys.Subtract))
            {
                int amplif = 1;

                if (shiftPressed && ctrlPressed)
                    amplif = 16;
                else if (shiftPressed)
                    amplif = 4;

                zoomAmount -= amplif * zoomApproach;
            }

            zoomAmount = Mathf.clamp(zoomAmount, -1, 1);
            MapHelper.Scene.camera.zoom = zoomAmount;

            #endregion

            #region Movement
            
            Vector2 mdir = new Vector2(MapHelper.HorizontalAxisInput(), MapHelper.VerticalAxisInput());
            Vector2 ms = MoveSpeed;
            int amplif2 = 1;

            if (shiftPressed && ctrlPressed)
                amplif2 = 12;
            if (shiftPressed)
                amplif2 = 4;
            
            mdir *= amplif2;
            ms *= mdir;

            if (altPressed)
            {
                MapHelper.ShiftAllTowards(ms);
                ms *= 0;
            }

            AddToPosition((int)ms.X, (int)ms.Y);
            
            #endregion

        }

        private void AddToPosition(int x, int y)
        {
            Vector2 newPos = entity.position;
            newPos.X += x;
            newPos.Y += y;

            // Check if we are inside the room bounds
            if (newPos.X < MapHelper.ScreenSize.X / 2)
                newPos.X = MapHelper.ScreenSize.X / 2;
            else if (newPos.X > editor.RoomSize.X - MapHelper.ScreenSize.X / 2)
                newPos.X = editor.RoomSize.X - MapHelper.ScreenSize.X / 2;

            if (newPos.Y < MapHelper.ScreenSize.Y / 2)
                newPos.Y = MapHelper.ScreenSize.Y / 2;
            else if (newPos.Y > editor.RoomSize.Y - MapHelper.ScreenSize.Y / 2)
                newPos.Y = editor.RoomSize.Y - MapHelper.ScreenSize.Y / 2;
            
            entity.position = newPos;
        }
        
        public void SetPosition(int x, int y)
        {
            Vector2 newPos = new Vector2(x,y);

            // Check if we are inside the room bounds
            if (newPos.X < MapHelper.ScreenSize.X / 2)
                newPos.X = MapHelper.ScreenSize.X / 2;
            else if (newPos.X > editor.RoomSize.X - MapHelper.ScreenSize.X / 2)
                newPos.X = editor.RoomSize.X - MapHelper.ScreenSize.X / 2;

            if (newPos.Y < MapHelper.ScreenSize.Y / 2)
                newPos.Y = MapHelper.ScreenSize.Y / 2;
            else if (newPos.Y > editor.RoomSize.Y - MapHelper.ScreenSize.Y / 2)
                newPos.Y = editor.RoomSize.Y - MapHelper.ScreenSize.Y / 2;

            entity.position = newPos;
        }
        

    }
}
