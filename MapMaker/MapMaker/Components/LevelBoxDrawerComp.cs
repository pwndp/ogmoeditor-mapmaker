﻿using MapMaker.Entities;
using Microsoft.Xna.Framework;
using Nez;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapMaker.Components
{
    public class LevelBoxDrawerComp : RenderableComponent
    {

        #region Fields

        LevelBoxEntity parent;
        Color roomBroder, solidsColor, exitsColor;

        #endregion

        #region Constructors and Overriden

        public override RectangleF bounds
        {
            get
            {
                if (_areBoundsDirty)
                {
                    setBounds();
                    _areBoundsDirty = false;
                }

                return _bounds;
            }
        }

        private void setBounds()
        {
            _bounds.calculateBounds(parent.position, Vector2.Zero, Vector2.Zero, Vector2.One, 0f, parent.level.Size.X, parent.level.Size.Y);
        }

        public override void onAddedToEntity()
        {
            base.onAddedToEntity();

            parent = entity as LevelBoxEntity;
            solidsColor = Color.White;
            exitsColor = Color.Magenta;
        }

        public override void render(Graphics graphics, Camera camera)
        {
            // Then draw the room border
            Rectangle rsize = new Rectangle(parent.position.ToPoint(), parent.level.Size.ToPoint());
            graphics.batcher.drawRect(rsize, GetRoomColor(parent.level.LevelType));

            // Everything is drawn relative to the parent
            Vector2 tl = parent.position;
            // Draw first the exits
            foreach (Rectangle rect in parent.exits)
                graphics.batcher.drawRect(new Vector2(tl.X + rect.X, tl.Y + rect.Y), rect.Width, rect.Height , exitsColor);
            // Then draw the solid (because we may draw over them, if that actually happens)
            foreach (Rectangle rect in parent.solids)
                graphics.batcher.drawRect(new Vector2(tl.X + rect.X, tl.Y + rect.Y), rect.Width, rect.Height, solidsColor);
        }

        #endregion

        #region Functions

        private Color GetRoomColor(LevelType type)
        {
            Color col;
            switch (type)
            {
                case LevelType.Normal:
                    col = Color.Gray;
                    break;
                case LevelType.Waprable:
                    col = Color.CadetBlue;
                    break;
                case LevelType.Darkness:
                    col = Color.DarkViolet;
                    break;
                default:
                case LevelType.Invalid:
                    col = Color.Red;
                    break;
            }
            return col;
        }

        #endregion

    }
}
