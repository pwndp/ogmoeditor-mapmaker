﻿using MapMaker.Entities;
using Microsoft.Xna.Framework;
using Nez;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapMaker.Components
{
    public class MouseInteract : Component, IUpdatable
    {

        #region Fields

        // References
        MapMakerScene editor;
        MouseFollower mouse;
        MouseData mouseData;

        // Variables
        LevelBoxEntity selected;
        Vector2 diffPos;

        #endregion

        #region Constructors and overriden

        public override void onAddedToEntity()
        {
            base.onAddedToEntity();

            editor = entity.scene as MapMakerScene;
            mouse = entity.getComponent<MouseFollower>();
            mouseData = new MouseData(mouse);

            diffPos = Vector2.Zero;
            selected = null;
        }

        public void update()
        {
            mouseData.UpdateFlags();

            // Check if we are colliding with a GUI element
            if (selected==null && CheckCollisionWithGUIElements())
            {
                InteractWithGUIElement();
                return;
            }

            // Picking up a level
            if (selected != null)
            {
                if (mouseData.LMBHold)
                    selected.SetPosition(mouseData.PositionFixed - diffPos);
                else
                {
                    if (selected != null)
                        selected.Drop();
                    selected = null;
                    diffPos = Vector2.Zero;
                }
            }
            else
            {
                if (mouseData.LMBClick)
                {
                    var lvl = MapHelper.GetLevelAtPosition(mouseData.Position);
                    if (lvl == null)
                        return;
                    selected = lvl;
                    diffPos = mouseData.PositionFixed - lvl.position;
                    selected.Take();
                }
            }
        }

        #endregion

        #region GUI Checking

        private bool CheckCollisionWithGUIElements()
        {
            return MapHelper.IsGUIElementAtPosition(mouseData.Position,true);
        }

        private void InteractWithGUIElement()
        {
            var pos = mouseData.Position;
            var obj = MapHelper.GetGUIElementAtPosition(pos,true);

            // In case of an error - return
            if (obj == null)
                return;

            if (mouseData.LMBClick)
                obj.OnMouseLeftClicked(pos);
            else if (mouseData.LMBHold)
                obj.OnMouseLeftHeld(pos);
            else if (mouseData.LMBRelease)
                obj.OnMouseLeftReleased(pos);

            if (mouseData.RMBClick)
                obj.OnMouseRightClicked(pos);
            else if (mouseData.RMBHold)
                obj.OnMouseRightHeld(pos);
            else if (mouseData.RMBRelease)
                obj.OnMouseRightReleased(pos);
            
            if (mouseData.IsNothingActive() && mouseData.Position != mouseData.OldPosition)
                obj.OnMouseHoverOver(pos);
        }

        #endregion
        
        #region Classes

        private class MouseData
        {
            MouseFollower follower;

            public Vector2 OldPosition, OldPositionFixed, Position, PositionFixed;
            public Vector2 ClickPosLeft, ClickPosRight, ReleasePosLeft, ReleasePosRight;
            public bool LMBClick, LMBRelease, LMBHold;
            public bool RMBClick, RMBRelease, RMBHold;
            public bool LMBClickPrev, LMBReleasePrev, LMBHoldPrev;
            public bool RMBClickPrev, RMBReleasePrev, RMBHoldPrev;

            public MouseData(MouseFollower follow)
            {
                follower = follow;
                ClickPosLeft = ClickPosRight = Vector2.Zero;
                ReleasePosLeft = ReleasePosRight = Vector2.Zero;
                OldPositionFixed = PositionFixed = Vector2.Zero;
                OldPosition = Position = Vector2.Zero;
                LMBClick = LMBRelease = LMBHold = false;
                RMBClick = RMBRelease = RMBHold = false;
                LMBClickPrev = LMBReleasePrev = LMBHoldPrev = false;
                RMBClickPrev = RMBReleasePrev = RMBHoldPrev = false;
            }

            public void UpdateFlags()
            {
                // Update position
                OldPosition = Position;
                Position = follower.mousePosition();
                OldPositionFixed = PositionFixed;
                PositionFixed = follower.mousePositionFixed();

                // Perserve previous events
                LMBClickPrev = LMBClick;
                RMBClickPrev = RMBClick;
                LMBHoldPrev = LMBHold;
                RMBHoldPrev = RMBHold;
                LMBReleasePrev = LMBRelease;
                RMBReleasePrev = RMBRelease;

                // Update new events
                LMBClick = Input.leftMouseButtonPressed;
                LMBHold = Input.leftMouseButtonDown;
                LMBRelease = Input.leftMouseButtonReleased;
                RMBClick = Input.rightMouseButtonPressed;
                RMBHold = Input.rightMouseButtonDown;
                RMBRelease = Input.rightMouseButtonReleased;

                // Update the click/release positions
                if (LMBClick)
                    ClickPosLeft = Position;
                else if (LMBRelease)
                    ReleasePosLeft = Position;

                if (RMBClick)
                    ClickPosRight = Position;
                else if (RMBRelease)
                    ReleasePosRight = Position;

            }

            public bool IsNothingActive()
            {
                return !(LMBClick || LMBHold || LMBRelease || RMBClick || RMBHold || RMBRelease);
            }
        }

        #endregion

    }
}
