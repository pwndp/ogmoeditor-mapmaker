﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Nez;

namespace MapMaker
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class MapMaker : Nez.Core
    {
        public static GameWindow gmWindow;

        public MapMaker(): base(1280,720,windowTitle:"Map Maker")
        {
            IsMouseVisible = true;
            gmWindow = Window;
            Window.AllowUserResizing = true;
        }
        
        protected override void Initialize()
        {
            base.Initialize();
            // 320x180
            MapHelper.SetScreenSize(1280, 720);
            Scene.setDefaultDesignResolution(1280, 720, Scene.SceneResolutionPolicy.BestFit, 80, 48);
            MapMakerScene scene = new MapMakerScene();
            Core.scene = scene as MapMakerScene;
            Core.exitOnEscapeKeypress = false;

        }
    }
}
