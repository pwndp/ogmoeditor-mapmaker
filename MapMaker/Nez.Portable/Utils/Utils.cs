﻿using Microsoft.Xna.Framework;
using System;
using System.Text;


namespace Nez
{
    /// <summary>
    /// utility methods that don't yet have a proper home that makes sense
    /// </summary>
    public static class Utils
    {
        public static string randomString(int size = 38)
        {
            var builder = new StringBuilder();

            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * Random.nextFloat() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }


        /// <summary>
        /// swaps the two object types
        /// </summary>
        /// <param name="first">First.</param>
        /// <param name="second">Second.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public static void swap<T>(ref T first, ref T second)
        {
            T temp = first;
            first = second;
            second = temp;
        }

        /// <summary>
        /// Checks if a point is inside a rectangle
        /// </summary>
        /// <param name="rect">The rectangular area we are checking against</param>
        /// <param name="point">The point we are checking for</param>
        /// <returns></returns>
        public static bool PointInRectangle(this Rectangle rect, Vector2 point)
        {

            if ((point.X >= rect.X && point.X <= (rect.X + rect.Width) &&
                (point.Y >= rect.Y && point.Y <= (rect.Y + rect.Height))))
                return true;

            return false;
        }

        public static Point ToPoint(this Vector2 vec)
        {
            return new Point((int)vec.X, (int)vec.Y);
        }

        public static Vector2 ToVec2(this Point point)
        {
            return new Vector2(point.X, point.Y);
        }

        public static Rectangle RectangleToScreenPos(this Camera cam, Rectangle rect)
        {
            Vector2 position = new Vector2(rect.X, rect.Y);
            Point nPos = cam.worldToScreenPoint(position).ToPoint();

            Vector2 size = new Vector2(rect.X + rect.Width, rect.Y + rect.Height);
            Point nSize = cam.worldToScreenPoint(position).ToPoint();

            return new Rectangle(nPos, nSize - nSize);
        }

    }
}

