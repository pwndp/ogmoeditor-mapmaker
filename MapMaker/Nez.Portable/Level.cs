﻿using Nez.Tiled;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nez
{
    public class Level : Scene
    {

        public void LoadLevelMap(string lvlName)
        {
            // Get the actual level
            string subfolder = getLevelSubfolder(lvlName);
            var tiledMap = content.Load<TiledMap>("levels\\" + subfolder + "\\" + lvlName);
            var tiledEntity = createEntity("tiled-map");
            var tmc = new TiledMapComponent(tiledMap, "main");
            tiledEntity.addComponent(tmc);
            // Parse all the level data
            parseLevelData(tiledMap);
        }

        private string getLevelSubfolder(string lvl)
        {
            string sub = "w";
            int lvlnum = Int32.Parse(lvl.Substring(3));

            if (lvlnum >= 0 && lvlnum < 30)
                sub += "1";
            else if (lvlnum >= 30 && lvlnum < 60)
                sub += "2";

            // Then finally return the string
            return sub;
        }

        private void parseLevelData(TiledMap map)
        {
            if (map.objectGroups.Count < 1)
                return;

            // Get "objects" layer
            var objectG = map.getObjectGroup("objects");
            if (objectG != null)
            {
                var sp = createEntity("playerspawn");
                var spawn = objectG.objectWithName("playerSpawnPoint");
                sp.transform.position = spawn.position;
            }

            
            

        }

        public virtual void createPlayer()
        {
        }


    }
}
